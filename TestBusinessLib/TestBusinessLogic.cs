﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ProjectManagerBusinessLib;
using ProjectManagerEntitiesLib;
namespace TestBusinessLib
{
    [TestFixture]
    public class TestBusinessLogic
    {
        [Test]
        public void TestUserGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestAddUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            User item = new User();
            item.FirstName = "April";
            item.LastName = "Joe";
            item.EmployeeId = 2019;            
            obj.AddUser(item);
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 4);

        }
        [Test]
        public void TestParentTaskGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllParentTask().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestAddParentTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            ParentTask item = new ParentTask();
            item.Parent_Task = "Angular";           
            obj.AddParentTask(item);
            int actual = obj.GetAllParentTask().Count;
            Assert.Greater(actual, 2);

        }
        [Test]
        public void TestDeleteUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            List<User> user =  obj.DeleteUser(41);          
            Assert.Greater(user.Count(), 1);

        }
        [Test]
        public void TestDeleteProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            List<Project> user = obj.DeleteProject(1082);
            Assert.Greater(user.Count(), 1);

        }
        [Test]
        public void TestDeleteTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            List<Task> task = obj.DeleteTask(10);
            Assert.Greater(task.Count(), 1);

        }
        [Test]
        public void TestProjectGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestAddProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Project item = new Project();
            item.Project_Name = "ProjectTestVL";
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now.AddDays(1);
            item.Priority = 12;
            item.Manager_ID = 1;
            obj.AddProject(item);
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 1);

        }
        [Test]
        public void TestUpdateProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Project item = new Project();
            item.Project_Name = "ProjectTestVL";
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now.AddDays(1);
            item.Priority = 12;
            item.Manager_ID = 1;
            obj.AddProject(item);
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 3);

        }
        [Test]
        public void TestUpdateUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            User item = new User();
            item.User_ID = 29;       
            item.FirstName = "April";
            item.LastName = "Joe";
            item.EmployeeId = 2019;
            obj.UpdateUser(item);
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 1);

        }

        [Test]
        public void TestGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllTask().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestGetByTaskId()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = obj.GetByTaskId(9);
            Assert.AreEqual(9, item.Task_ID);
        }
        [Test]
        public void TestAddTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = new Task();
            item.TaskName = "APITest13";            
            item.Priority = 20;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now; 
            item.Status = 0;
            item.Project_ID = 3;
            item.TASK_OWNER_ID = 3;
            obj.AddTask(item);
            Task test = obj.GetByTaskName("APITest13");
            Assert.AreEqual("APITest13", test.TaskName);

        }
        //[Test]
        //public void TestDeleteTask()
        //{

        //    ProjectManagerBusiness obj = new ProjectManagerBusiness();
        //    obj.DeleteTask(6024);
        //    Task item = obj.GetByTaskId(6024);
        //    Assert.AreEqual(null, item);
        //}
        [Test]
        public void TestUpdateTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = new Task();
            item.Task_ID = 9;
            item.TaskName = "APITest21";
            item.Priority = 20;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Project_ID = 3;
            item.TASK_OWNER_ID = 3;
            item.Status = 0;
            obj.UpdateTask(item);
            Task itemafterupdate = obj.GetByTaskName("APITest21");
            Assert.AreEqual(20, itemafterupdate.Priority);

        }
        [Test]
        public void TestUpdateEndTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = new Task();
            item.Task_ID = 9;
            item.TaskName = "APITest21";
            item.Priority = 20;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Status = 1;
            obj.UpdateEndDate(item);
            Task itemafterupdate = obj.GetByTaskName("APITest21");
            Assert.AreEqual(1, itemafterupdate.Status);


        }
    

}
}
