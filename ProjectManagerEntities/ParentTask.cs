﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace ProjectManagerEntitiesLib
{
    public class ParentTask
    {
        [Key]
        public int Parent_ID { get; set; }
        [Required]
        public string Parent_Task { get; set; }
    }
}
