﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectManagerEntitiesLib
{
    public class Project
    {
        [Key]
        public int Project_ID { get; set; }
        [Required]
        public string Project_Name { get; set; }
        [Required]
        public int Priority { get; set; }

        [DataType(DataType.Date)]
        public  DateTime? Start_Date { get; set; }

        [DataType(DataType.Date)]
        public DateTime? End_Date { get; set; }
       
     
        public int Manager_ID { get; set; }

        [ForeignKey("Manager_ID")]
        public User User { get; set; }

    }
}
