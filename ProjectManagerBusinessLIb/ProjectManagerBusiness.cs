﻿using ProjectManagerDataLib;
using ProjectManagerEntitiesLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ProjectManagerBusinessLib
{
    public class ProjectManagerBusiness
    {
        public void AddParentTask(ParentTask item)
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                dbcontext.ParentTasks.Add(item);
                dbcontext.SaveChanges();
            }
        }
        public List<ParentTask> GetAllParentTask()
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {

                return dbcontext.ParentTasks.ToList();
            }
        }

        public List<User> AddUser(User item)
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {               
                dbcontext.Users.Add(item);
                dbcontext.SaveChanges();
                return dbcontext.Users.ToList();
                    
            }
        }
        public List<User> DeleteUser(int id)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var user = ctx.Users
                    .Where(s => s.User_ID == id)
                    .FirstOrDefault();

                ctx.Entry(user).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Users.ToList();
            }

        }

        public void UpdateUser(User item)
        {
            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                var context = dbcontext.Users.SingleOrDefault(x => x.User_ID == item.User_ID);
                context.User_ID = item.User_ID;
                context.EmployeeId = item.EmployeeId;
                context.FirstName = item.FirstName;
                context.LastName = item.LastName;             
                dbcontext.SaveChanges();

            }

        }
        public List<User> GetAllUser()
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {

                return dbcontext.Users.ToList();
            }
        }

        public int AddProject(Project item)
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                dbcontext.Projects.Add(item);
                dbcontext.SaveChanges();
                int id = item.Project_ID;
                return id;
            }
        }
        public List<Project> DeleteProject(int id)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var project = ctx.Projects
                    .Where(s => s.Project_ID == id)
                    .FirstOrDefault();

                ctx.Entry(project).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Projects.ToList();
            }

        }

        public void UpdateProject(Project item)
        {
            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                var context = dbcontext.Projects.SingleOrDefault(x => x.Project_ID == item.Project_ID);
                context.Project_Name = item.Project_Name;
                context.Priority = item.Priority;
                context.Start_Date = item.Start_Date;
                context.End_Date = item.End_Date;
                context.Manager_ID = item.Manager_ID;
                dbcontext.SaveChanges();

            }

        }
        public List<Project> GetAllProject()
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {

                return dbcontext.Projects.ToList();
            }
        }

        public int AddTask(Task item)
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                dbcontext.Tasks.Add(item);
                dbcontext.SaveChanges();
                int id = item.Task_ID;
                return id;
            }
        }
        public List<Task> DeleteTask(int id)
        {
            using (var ctx = new ProjectManagerContext())
            {
                var task = ctx.Tasks
                    .Where(s => s.Task_ID == id)
                    .FirstOrDefault();

                ctx.Entry(task).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();

                return ctx.Tasks.ToList();
            }

        }
        public List<Task> UpdateEndDate(Task item)
        {
            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.Task_ID == item.Task_ID);
                context.End_Date = System.DateTime.Now;
                context.Status = 1;
                dbcontext.SaveChanges();
                return dbcontext.Tasks.ToList();

            }

        }
        public void UpdateTask(Task item)
        {
            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.Task_ID == item.Task_ID);                
                context.TaskName = item.TaskName;
                context.Parent_ID = item.Parent_ID;
                context.Project_ID = item.Project_ID;
                context.TASK_OWNER_ID = item.TASK_OWNER_ID;
                context.Priority = item.Priority;
                context.Start_Date = item.Start_Date;
                context.End_Date = item.End_Date;
                context.Status = item.Status;
                dbcontext.SaveChanges();

            }

        }
        public List<Task> GetAllTask()
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {

                return dbcontext.Tasks.ToList();
            }
        }
        public Task GetByTaskName(string TaskName)
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.TaskName == TaskName);
                return context;
            }

        }
        public Task GetByTaskId(int TaskId)
        {

            using (ProjectManagerContext dbcontext = new ProjectManagerContext())
            {
                var context = dbcontext.Tasks.SingleOrDefault(x => x.Task_ID == TaskId);
                return context;
            }

        }

    }
}
